# Welcome to Online Shop project!

__This project is a training, with the purpose to get fully ready for the internship at Endava.__

The starting point of work here is the final project of the __Front End Web Developer__ course ([Codifun](Front End Web Developer.)).

### Training objectives ([Endava requirements](https://endava.taleo.net/careersection/2/jobdetail.ftl?job=DEV001QL&lang=en))
  1. General IT (PC architecture, networking, operating systems etc.) 
  1. Programming (one of the modern languages: Java, C#, JavaScript, etc)
  1. Algorithms and data structures (sorting, searching, trees)
  1. OOP (Concepts, principles, patterns)
  1. Database (SQL, DBMS)
  
### Training roadmap
  1. SQL: CRUD commands (Postgres)
  1. Version control (Git, Bitbucket)
  1. Project planning/tracking (Trello)
  1. Project build (Jenkins, Bitbucket pipeline)
  1. Java
      1. Installation
      1. IntelliJ
      1. Hello World!
      1. OOP principles
      1. Develop the model for the project's domain
      1. Add functionality to the model
      1. TDD basics (Test Driven Development)
      1. Develop the persistence layer
      1. Functional testing basics
      1. Add more complex functionality using Java API data structures (ArrayList, Map). 
  1. Algorithms
      1. Develop a sorting algorithm (Java)
      1. Develop a searching algorithm (Java)
      1. Concepts of the algorithm cost and the big O.
  1. Connecting the Front-End with the Back-End
  1. Application deployment